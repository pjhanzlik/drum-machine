import React, {useState, useRef, useReducer} from 'react';
import AudioButton from './components/AudioButton';
import './App.css';

function init(keys) {
  const STATE = {};
  for(const KEY of keys) {
    STATE[KEY] = false;
  }
  return STATE;
}

function reducer(state, action) {
  return {...state, [action.type]: !state[action.type]};
}

export default function App(props) {
  const TIMES = [
    "first",
    "second",
    "third",
    "fourth",
    "fifth",
    "sixth",
    "seventh",
    "eighth",
    "ninth"
  ]

  const CLIPS = {
    "Q": useRef(null),
    "W": useRef(null),
    "E": useRef(null),
    "A": useRef(null),
    "S": useRef(null),
    "D": useRef(null),
    "Z": useRef(null),
    "X": useRef(null),
    "C": useRef(null)
  };

  const [state, dispatch] = useReducer(reducer, Object.keys(CLIPS), init)
  const [last, setLast] = useState("9 second drumkit");

  const DRUMPADS = Object.keys(CLIPS).map( (key, i) =>
    (
      <AudioButton
        key={key}
        value={key}
        audio={CLIPS[key]}
        src={`https://upload.wikimedia.org/wikipedia/commons/5/5f/Opus_One_audio.flac#t=${i},${i+1}`}
        onMouseDown={playClip(key)}
        active={state[key]}
      />
    )
  );

  function playClip(key) {
    return () => {
      CLIPS[key].current.load();
      CLIPS[key].current.play();
      setLast(`${TIMES[Object.keys(CLIPS).indexOf(key)]} Second`);
    }
  }

  function handleKeyDown(e) {
    const KEY = e.key.toUpperCase();
    if(CLIPS.hasOwnProperty(KEY) && !state[KEY]) {
      dispatch({type: KEY});
      playClip(KEY)();
    }
  }

  function handleKeyUp(e) {
    const KEY = e.key.toUpperCase();
    if(CLIPS.hasOwnProperty(KEY)) {
      dispatch({type: KEY})
    }
  }

  return (
    <div
      className="App"
      id="drum-machine"
      onKeyDown={handleKeyDown}
      onKeyUp={handleKeyUp}
      tabIndex="0"
    >
      <div id="display">
        {DRUMPADS}
        <p>{last.toUpperCase()}</p>
      </div>
    </div>
  );
}