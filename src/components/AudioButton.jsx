import React, { useState } from 'react';

export default function AudioButton(props) {
    const [playing, setPlaying] = useState(false);

    function handlePlay(e) {
        setPlaying(true);
    }

    function handlePause() {
        setPlaying(false);
    }

    return (
        <button
            className={
                `drum-pad
                ${playing ? "playing" : ""}
                ${props.active ? "active" : ""}`
            }
            id={props.src}
            type="button"
            onMouseDown={props.onMouseDown}
        >
            {props.value}
            <audio
                className="clip"
                id={props.value} 
                src={props.src}
                preload="auto"
                ref={props.audio}
                onPause={handlePause}
                onPlay={handlePlay}
            />
        </button>
    );
}