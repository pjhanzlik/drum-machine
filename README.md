This is a drumkit made as coursework for freeCodeCamp's Front End Libraries
Developer Certification. In addition to fulfilling
[required user stories](https://learn.freecodecamp.org/front-end-libraries/front-end-libraries-projects/build-a-drum-machine),
I wanted to emulate the 
[example freeCodeCamp web page](https://codepen.io/freeCodeCamp/full/MJyNMd)
styling with CSS queries which scale down to
[240px width devices](https://www.kaiostech.com/meet-the-devices-that-are-powered-by-kaios/).

Probably the most interesting bit of this project is how a simple React
useReducer call nicely handles keypress actions. Other than that, the drumkit
buttons will be highlighted when child audio plays using synthetic React media
events.

This site has been manually tested to render nicely on mobile and desktop
[WebKit](https://webkit.org/), [Blink](https://www.chromium.org/blink), and
[Servo](https://servo.org/) powered browsers, but your milage may vary.
If you do spot a rendering problem, please
[create a new Issue](https://gitlab.com/pjhanzlik/drum-machine/issues)
with a screenshot of the problem and a description of your hardware.
